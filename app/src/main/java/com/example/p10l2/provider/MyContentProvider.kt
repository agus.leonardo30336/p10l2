package com.example.p10l2.provider

import android.content.ContentProvider
import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.net.Uri
import com.example.p10l2.MyDatabase

class MyContentProvider : ContentProvider() {

    override fun onCreate(): Boolean {
        return true
    }

    override fun query(
        p0: Uri,
        p1: Array<out String>?,
        p2: String?, p3: Array<out String>?,
        p4: String?
    ): Cursor {
        val database = MyDatabase.getDatabase(context!!).userDao()
        val cursor = database.getData()
        cursor.setNotificationUri(context!!.contentResolver, p0)
        return cursor
    }

    override fun insert(p0: Uri?, p1: ContentValues?): Uri {
        TODO("not implemented")
    }

    override fun update(
        p0: Uri?,
        p1: ContentValues?,
        p2: String?,
        p3: Array<out String>?
    ): Int {
        val roomDatabase = MyDatabase.getDatabase(context!!).openHelper.writableDatabase
        return roomDatabase.update("user", SQLiteDatabase.CONFLICT_NONE, p1, p2, p3)
    }

    override fun delete(
        p0: Uri?,
        p1: String?,
        p2: Array<out String>?
    ): Int {
        val roomDatabase = MyDatabase.getDatabase(context!!).openHelper.writableDatabase
        return roomDatabase.delete("user", p1, p2)
    }

    override fun getType(p0: Uri?): String {
        TODO("not implemented")
    }

    companion object {
        const val AUTHORITY = "com.example.p10l2.provider.MyContentProvider"
        val CONTENT_URI: Uri = Uri.parse("content://$AUTHORITY/user")
    }
}

