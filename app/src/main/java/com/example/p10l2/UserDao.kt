package com.example.p10l2

import android.database.Cursor
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface UserDao {
    @Query("SELECT * FROM user")
    fun getAll(): List<User>

    @Query("SELECT * FROM user")
    fun getData(): Cursor

    @Insert
    fun insertUser(user: User)

    @Query("DELETE FROM User WHERE id = :id")
    fun deleteUser(id: Int)

    @Query("UPDATE User SET name = :name, email = :email, phone = :phone WHERE id = :id")
    fun updateUser(id: Int, name: String, email: String, phone: String): Int

    @Query("DELETE FROM User")
    fun nukeTable()
}