package com.example.p10l2

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var database: MyDatabase
    lateinit var userList: List<User>
    lateinit var dataAdapter: ArrayAdapter<String>
    lateinit var nameList: ArrayList<String>
    lateinit var idList: ArrayList<Int>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initObject()
        getUserList()
        initSpinner()
        initListener()
    }

    private fun initObject() {
        database = MyDatabase.getDatabase(this)
        nameList = ArrayList()
        idList = ArrayList()
    }

    private fun initListener() {
        btn_submit.setOnClickListener {
            if (btn_submit.text == "Simpan") {
                database.userDao().insertUser(
                    User(
                        name = et_name.text.toString(),
                        email = et_email.text.toString(),
                        phone = et_phone_number.text.toString()
                    )
                )
            } else {
                database.userDao().updateUser(
                    idList[spinner1.selectedItemPosition],
                    et_name.text.toString(),
                    et_email.text.toString(),
                    et_phone_number.text.toString()
                )
            }

            et_name.text.clear()
            et_email.text.clear()
            et_phone_number.text.clear()

            getUserList()
            dataAdapter.notifyDataSetChanged()

            btn_submit.text = "Simpan"
        }

        btn_reset.setOnClickListener {
            database.userDao().deleteUser(idList[spinner1.selectedItemPosition])

            et_name.text.clear()
            et_email.text.clear()
            et_phone_number.text.clear()

            getUserList()
            dataAdapter.notifyDataSetChanged()

            btn_submit.text = "Simpan"
        }

        spinner1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                et_name.text.clear()
                et_email.text.clear()
                et_phone_number.text.clear()

                btn_submit.text = "Simpan"
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                et_name.setText(userList[position].name)
                et_email.setText(userList[position].email)
                et_phone_number.setText(userList[position].phone)

                btn_submit.text = "Ubah"
            }
        }
    }

    private fun initSpinner() {
        dataAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, nameList)
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner1.adapter = dataAdapter
    }

    private fun getUserList() {
        userList = database.userDao().getAll()
        idList.clear()
        nameList.clear()

        for (i in userList.indices) {
            userList[i].name?.let { nameList.add(it) }
            userList[i].id?.let { idList.add(it) }
        }
    }

}
